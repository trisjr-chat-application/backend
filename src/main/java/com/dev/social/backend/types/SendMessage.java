package com.dev.social.backend.types;

import com.dev.social.backend.dtos.MessageDto;
import com.dev.social.backend.dtos.RoomDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SendMessage {
    private MessageDto message;
    private RoomDto room;
}
