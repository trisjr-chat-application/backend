package com.dev.social.backend.types;

import com.dev.social.backend.dtos.MessageDto;
import com.dev.social.backend.dtos.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FriendListItem {
    private UserDto user;
    private MessageDto lastMessage;
}
