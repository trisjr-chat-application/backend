package com.dev.social.backend.types;

import com.dev.social.backend.dtos.MessageDto;
import com.dev.social.backend.dtos.MessageStatusDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
public class ChatItem implements Serializable {
    private String key;
    private String photo;
    private String name;
    private MessageDto lastMessage;
    private MessageStatusDto messageStatus;
    private Boolean isSeen;
}
