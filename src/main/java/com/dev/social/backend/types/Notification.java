package com.dev.social.backend.types;

import com.dev.social.backend.enums.NotificationStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Notification {
    private String content;
    private NotificationStatusEnum status;
}
