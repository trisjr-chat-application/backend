package com.dev.social.backend.types;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class UrlPreview {
    private String title;
    private String description;
    private String image;
    private String url;
}
