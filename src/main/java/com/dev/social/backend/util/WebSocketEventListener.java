package com.dev.social.backend.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.util.Objects;

@Component
public class WebSocketEventListener {
    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);
    private final SimpMessagingTemplate messagingTemplate;

    public WebSocketEventListener(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("Received a new web socket connection");
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        logger.info("Received a new web socket disconnect");
    }

    @EventListener
    public void handleWebSocketSubscribeListener(SessionSubscribeEvent event) {
        switch (Objects.requireNonNull(event.getMessage().getHeaders().get("simpDestination")).toString()) {
            case "/queue/messages" -> logger.info("Received a new web socket subscribe in /queue/messages");
            case "/queue/notifications" -> logger.info("Received a new web socket subscribe in /queue/notifications");
            default ->
                    logger.info("Received a new web socket subscribe in {}", event.getMessage().getHeaders().get("simpDestination"));
        }
    }

    @EventListener
    public void handleWebSocketSendListener(SessionSubscribeEvent event) {
        // Send event
        logger.info("Received a new web socket send");
    }
}
