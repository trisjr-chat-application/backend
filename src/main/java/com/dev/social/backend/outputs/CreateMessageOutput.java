package com.dev.social.backend.outputs;

import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateMessageOutput {
    private Message message;
    private List<User> users;
    private String roomKey;
}
