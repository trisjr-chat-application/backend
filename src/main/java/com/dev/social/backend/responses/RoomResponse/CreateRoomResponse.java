package com.dev.social.backend.responses.RoomResponse;

import com.dev.social.backend.dtos.RoomDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateRoomResponse {
    private RoomDto room;
}
