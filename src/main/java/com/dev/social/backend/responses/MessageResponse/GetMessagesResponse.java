package com.dev.social.backend.responses.MessageResponse;

import com.dev.social.backend.dtos.MessageDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetMessagesResponse {
    private List<MessageDto> messages;
}
