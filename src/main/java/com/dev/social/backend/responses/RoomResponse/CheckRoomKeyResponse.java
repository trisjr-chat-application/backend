package com.dev.social.backend.responses.RoomResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CheckRoomKeyResponse {
    private boolean isRoomKeyValid;
}
