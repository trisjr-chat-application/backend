package com.dev.social.backend.responses.MessageResponse;

import com.dev.social.backend.dtos.UserAndRoomDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ReadMessageResponse {
    private String roomKey;
    private List<UserAndRoomDto> users;
}
