package com.dev.social.backend.responses.FileResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UploadFileDataResponse {
    private String fileName;
    private String filePath;
    private String fileType;
    private String fileUrl;
}
