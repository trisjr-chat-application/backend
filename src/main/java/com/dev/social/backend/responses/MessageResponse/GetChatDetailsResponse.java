package com.dev.social.backend.responses.MessageResponse;

import com.dev.social.backend.dtos.MessageDto;
import com.dev.social.backend.dtos.UserAndRoomDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetChatDetailsResponse {
    private Object chatDetail;
    private List<MessageDto> messages;
    private List<UserAndRoomDto> users;
}
