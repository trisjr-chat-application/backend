package com.dev.social.backend.responses.RoomResponse;

import com.dev.social.backend.dtos.RoomDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetRoomInvitedResponse {
    List<RoomDto> rooms;
}
