package com.dev.social.backend.services.role;

import com.dev.social.backend.enums.RoleCodeEnum;
import com.dev.social.backend.models.Role;

public interface RoleService {
    Role getByRoleCode(RoleCodeEnum role);
}
