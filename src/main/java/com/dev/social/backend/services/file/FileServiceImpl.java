package com.dev.social.backend.services.file;

import com.dev.social.backend.models.File;
import com.dev.social.backend.repositories.FileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class FileServiceImpl implements FileService {
    private final FileRepository fileRepository;

    public FileServiceImpl(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    @Override
    public File saveFile(File file) {
        return fileRepository.save(file);
    }

    @Override
    public File getFile(String filePath) {
        return fileRepository.findByPath(filePath);
    }

    @Override
    public Boolean isFileExpired(File file) {
        if (file == null)
            return true;
        return file.getLimitTime().before(new Date());
    }
}
