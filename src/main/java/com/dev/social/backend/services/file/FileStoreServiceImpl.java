package com.dev.social.backend.services.file;

import com.dev.social.backend.models.File;
import com.dev.social.backend.responses.FileResponse.UploadFileDataResponse;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Bucket;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.StorageClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class FileStoreServiceImpl implements FileStoreService {
    private final FileService fileService;

    public FileStoreServiceImpl(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public Bucket getBucket() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("src/main/resources/credentials.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setStorageBucket("chatappstore-2c51b.appspot.com")
                .build();
        boolean hasBeenInitialized = false;
        for (FirebaseApp app : FirebaseApp.getApps()) {
            if (app.getName().equals(FirebaseApp.DEFAULT_APP_NAME)) {
                hasBeenInitialized = true;
            }
        }
        if (!hasBeenInitialized) {
            FirebaseApp.initializeApp(options);
        }

        return StorageClient.getInstance().bucket();
    }

    @Override
    public UploadFileDataResponse uploadFile(MultipartFile file) throws IOException {
        byte[] fileBytes = file.getBytes();
        Bucket bucket = getBucket();
        String fileContentType = file.getContentType();
        assert fileContentType != null;
        String fileName = file.getOriginalFilename();
        assert fileName != null;
        String filePath = fileContentType + "/" + System.currentTimeMillis() + "_" + fileName.replaceAll("\\s+", "_").toLowerCase();
        bucket.create(filePath, fileBytes, fileContentType);

        Long size = bucket.get(filePath).getSize();
        Date limitTime = new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(100));

        File fileModel = File.builder()
                .name(fileName)
                .path(filePath)
                .type(fileContentType)
                .url(getSingedUrl(filePath))
                .size(size)
                .limitTime(limitTime)
                .build();

        fileService.saveFile(fileModel);

        return UploadFileDataResponse.builder()
                .fileType(fileContentType)
                .fileName(fileName)
                .filePath(filePath)
                .fileUrl(getSingedUrl(filePath))
                .build();
    }

    @Override
    public String convertToBase64(byte[] file) {
        return Base64Utils.encodeToString(file);
    }

    @Override
    public String getSingedUrl(String filePath) {
        try {
            Bucket bucket = getBucket();
            return bucket.get(filePath).signUrl(100, TimeUnit.DAYS).toString();
        } catch (IOException e) {
            return null;
        }

    }

    @Override
    @SneakyThrows
    public File getFile(String filePath) {
        File file = fileService.getFile(filePath);
        if (file != null && fileService.isFileExpired(file)) {
            String url = getSingedUrl(filePath);
            Date limitTime = new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(100));
            file.setUrl(url);
            file.setLimitTime(limitTime);
            file = fileService.saveFile(file);
        }
        return file;
    }
}
