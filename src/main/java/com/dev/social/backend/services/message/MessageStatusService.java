package com.dev.social.backend.services.message;

import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.MessageStatus;
import com.dev.social.backend.models.User;

import java.util.List;

public interface MessageStatusService {
    MessageStatus createMessageStatus(User user, Message message);

    Message save(MessageStatus messageStatus);

    MessageStatus getMessageStatusByUserAndMessage(User user, Message message);

    void createMessageStatuses(List<User> users, Message message);
}
