package com.dev.social.backend.services.helpers;

import com.dev.social.backend.types.UrlPreview;

import java.io.IOException;

public interface Helper {
    UrlPreview getLinkPreview(String text) throws IOException;

    String getUrlFromText(String text);
}
