package com.dev.social.backend.services.user;

import com.dev.social.backend.models.Relationship;
import com.dev.social.backend.models.User;

import java.util.List;

public interface RelationshipService {
    Relationship createRelationship(User sender, User receiver);

    List<User> getFriends(User user, String key);

    List<User> getAllSentRequest(User user);

    List<User> getAllReceivedRequest(User user);

    Relationship getRelationship(User sender, User receiver);

    void save(Relationship relationship);

    void delete(Relationship relationship);

    Boolean isFriend(User user, User friend);
}
