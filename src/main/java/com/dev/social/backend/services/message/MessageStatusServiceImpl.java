package com.dev.social.backend.services.message;

import com.dev.social.backend.enums.ReactionEnum;
import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.MessageStatus;
import com.dev.social.backend.models.User;
import com.dev.social.backend.repositories.MessageStatusRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class MessageStatusServiceImpl implements MessageStatusService {
    private final MessageStatusRepository messageStatusRepository;

    @Override
    public MessageStatus createMessageStatus(User user, Message message) {
        MessageStatus messageStatus = MessageStatus.builder()
                .user(user)
                .message(message)
                .reaction(ReactionEnum.NONE)
                .build();
        messageStatusRepository.save(messageStatus);

        return messageStatus;
    }

    @Override
    public Message save(MessageStatus messageStatus) {
        return messageStatusRepository.save(messageStatus).getMessage();
    }

    @Override
    public MessageStatus getMessageStatusByUserAndMessage(User user, Message message) {
        return messageStatusRepository.findByUserAndMessage(user, message);
    }

    @Override
    public void createMessageStatuses(List<User> users, Message message) {
        users.forEach(user -> createMessageStatus(user, message));
    }
}
