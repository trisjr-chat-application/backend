package com.dev.social.backend.services.file;

import com.dev.social.backend.models.File;

public interface FileService {
    File saveFile(File file);

    File getFile(String filePath);

    Boolean isFileExpired(File file);
}
