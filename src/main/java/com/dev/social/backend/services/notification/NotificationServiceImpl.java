package com.dev.social.backend.services.notification;

import com.dev.social.backend.enums.NotificationStatusEnum;
import com.dev.social.backend.types.Notification;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService {

    public NotificationServiceImpl() {
    }

    @Override
    public Notification createNotification(String content, NotificationStatusEnum status) {
        return Notification.builder()
                .content(content)
                .status(status)
                .build();
    }
}
