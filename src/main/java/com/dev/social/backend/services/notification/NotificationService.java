package com.dev.social.backend.services.notification;

import com.dev.social.backend.enums.NotificationStatusEnum;
import com.dev.social.backend.types.Notification;

public interface NotificationService {
    Notification createNotification(String content, NotificationStatusEnum status);
}
