package com.dev.social.backend.services.message;

import com.dev.social.backend.enums.MemberGroupStatusEnum;
import com.dev.social.backend.enums.MessageEnum;
import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.models.UserAndRoom;
import com.dev.social.backend.outputs.CreateMessageOutput;
import com.dev.social.backend.repositories.MessageRepository;
import com.dev.social.backend.requests.MessageRequest.SendMessageRequest;
import com.dev.social.backend.services.room.RoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class MessageServiceImpl implements MessageService {
    private final MessageRepository messageRepository;
    private final MessageStatusService messageStatusService;
    private final RoomService roomService;

    public MessageServiceImpl(MessageRepository messageRepository, MessageStatusService messageStatusService, RoomService roomService) {
        this.messageRepository = messageRepository;
        this.messageStatusService = messageStatusService;
        this.roomService = roomService;
    }

    @Override
    public CreateMessageOutput createMessage(User sender, SendMessageRequest request) {
        String content = request.getContent();
        MessageEnum type = request.getType();
        String key = request.getKey();
        Message replyMessage = request.getReplyMessageId() != null ? getMessageById(request.getReplyMessageId()) : null;
        // Create message
        Room room = roomService.getRoomByKey(key);
        Message message = Message.builder()
                .content(content)
                .type(type)
                .sender(sender)
                .replyMessage(replyMessage)
                .room(room)
                .build();
        Message savedMessage = messageRepository.save(message);

        // Create message statuses
        List<User> users = room.getUsersInvited().stream().filter(userAndRoom -> userAndRoom.getStatus() == MemberGroupStatusEnum.JOINED).map(UserAndRoom::getUser).toList();
        messageStatusService.createMessageStatuses(users, savedMessage);

        // Save last message to room
        roomService.save(room);

        return CreateMessageOutput.builder()
                .users(users)
                .message(message)
                .roomKey(key)
                .build();
    }

    @Override
    public List<Message> getAllMessagesToRoom(Room room, Pageable pageable) {
        return messageRepository.findAllByRoom(room, pageable);
    }

    @Override
    public Message save(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Message createMessageNotificationToRoom(Room room, String content) {
        Message message = Message.builder()
                .content(content)
                .type(MessageEnum.NOTIFICATION)
                .room(room)
                .build();
        Message savedMessage = messageRepository.save(message);
        roomService.save(room);
        return savedMessage;
    }

    @Override
    public Message getMessageById(Long id) {
        return messageRepository.findById(id).orElse(null);
    }

    @Override
    public List<Message> getAllMessagesToRoomWithType(Room room, MessageEnum type) {
        return messageRepository.findAllByRoomAndType(room, type);
    }
}
