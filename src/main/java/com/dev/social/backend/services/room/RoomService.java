package com.dev.social.backend.services.room;

import com.dev.social.backend.enums.RoomTypeEnum;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.types.ChatItem;

import java.util.List;

public interface RoomService {

    Room getRoomByKey(String key);

    Room createRoom(String name, String key, String photo, RoomTypeEnum type);

    Room save(Room room);

    Room createPrivateRoom(List<User> users);

    ChatItem getChatItem(User user, Room room);
}
