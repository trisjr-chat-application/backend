package com.dev.social.backend.services.helpers;

import com.dev.social.backend.dtos.*;
import com.dev.social.backend.enums.GenderEnum;
import com.dev.social.backend.enums.MessageEnum;
import com.dev.social.backend.enums.RelationshipStatusEnum;
import com.dev.social.backend.enums.RoomTypeEnum;
import com.dev.social.backend.models.*;
import com.dev.social.backend.services.file.FileStoreService;
import com.dev.social.backend.types.UrlPreview;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ConverterImpl implements Converter {
    private final FileStoreService fileStoreService;
    private final Helper helper;

    public ConverterImpl(FileStoreService fileStoreService, Helper helper) {
        this.fileStoreService = fileStoreService;
        this.helper = helper;
    }

    @Override
    public UserDto convertUserToUserDto(User user) {
        if (user == null) return null;
        Long id = user.getId();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String email = user.getEmail();
        String phoneNumber = user.getPhoneNumber();
        Date birthDate = user.getBirthDate();
        String address = user.getAddress();
        String city = user.getCity();
        String country = user.getCountry();
        String profilePicture = user.getProfilePicture();
        String coverPicture = user.getCoverPicture();
        String aboutMe = user.getAboutMe();
        GenderEnum gender = user.getGender();
        RelationshipStatusEnum relationshipStatus = user.getRelationshipStatus();

        return new UserDto(
                id, firstName, lastName, email, phoneNumber, birthDate, address, city, country, profilePicture, coverPicture, aboutMe, gender, relationshipStatus
        );
    }

    @SneakyThrows
    @Override
    public MessageDto convertMessageToMessageDto(Message message) {
        if (message == null) return null;
        Set<MessageStatusDto> messageStatuses = null;
        if (message.getType() != MessageEnum.NOTIFICATION && message.getMessageStatuses().size() > 0) {
            messageStatuses = message.getMessageStatuses().stream().map(this::convertMessageStatusToMessageStatusDto).collect(Collectors.toSet());
        }
        String content = message.getContent();
        File file = null;
        UrlPreview urlPreview = null;
        MessageEnum type = message.getType();
        switch (type) {
            case IMAGE -> {
                file = fileStoreService.getFile(message.getContent());
                content = "Sent an image";
            }
            case FILE -> {
                file = fileStoreService.getFile(message.getContent());
                content = "Sent a file";
            }
            case TEXT -> urlPreview = helper.getLinkPreview(message.getContent());
            case VIDEO -> {
                file = fileStoreService.getFile(message.getContent());
                content = "Sent a video";
            }
        }
        return new MessageDto(
                message.getId(),
                content,
                convertUserToUserDto(message.getSender()),
                message.getRoom().getRoomKey(),
                message.getType(),
                convertMessageToMessageDto(message.getReplyMessage()),
                messageStatuses,
                message.getCreatedAt(),
                message.getUpdatedAt(),
                urlPreview,
                file
        );
    }

    @Override
    public MessageStatusDto convertMessageStatusToMessageStatusDto(MessageStatus messageStatus) {
        if (messageStatus == null) return null;

        return new MessageStatusDto(
                messageStatus.getId(),
                convertUserToUserDto(messageStatus.getUser()),
                messageStatus.getReaction(),
                messageStatus.getCreatedAt(),
                messageStatus.getUpdatedAt()
        );
    }

    @SneakyThrows
    @Override
    public RoomDto convertRoomToRoomDto(Room room, User user) {
        if (room == null) return null;

        Set<UserAndRoomDto> usersInvited = room.getUsersInvited().stream().map(this::convertUserAndRoomToUserAndRoomDto).collect(Collectors.toSet());

        String photo = "";
        String name = room.getName();
        if (room.getType() == RoomTypeEnum.PRIVATE) {
            for (User u : room.getUsersInvited().stream().map(UserAndRoom::getUser).collect(Collectors.toSet())) {
                if (!u.getId().equals(user.getId())) {
                    name = u.getFirstName() + " " + u.getLastName();
                    // Check profilePicture is link or not
                    if (u.getProfilePicture() != null && !u.getProfilePicture().isEmpty() && !u.getProfilePicture().contains("http"))
                        photo = fileStoreService.getFile(u.getProfilePicture()).getUrl();
                    else
                        photo = u.getProfilePicture();
                }
            }
        } else {
            File file = fileStoreService.getFile(room.getImage());
            if (file != null) photo = file.getUrl();
        }

        return new RoomDto(
                room.getId(),
                room.getRoomKey(),
                name,
                photo,
                room.getType(),
                usersInvited,
                room.getCreatedAt(),
                room.getUpdatedAt()
        );
    }

    @Override
    public UserAndRoomDto convertUserAndRoomToUserAndRoomDto(UserAndRoom userAndRoom) {
        if (userAndRoom == null) return null;

        return new UserAndRoomDto(
                userAndRoom.getId(),
                convertUserToUserDto(userAndRoom.getUser()),
                userAndRoom.getIsCreator(),
                userAndRoom.getStatus(),
                userAndRoom.getLastSeenAt(),
                userAndRoom.getCreatedAt(),
                userAndRoom.getUpdatedAt()
        );
    }
}
