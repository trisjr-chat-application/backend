package com.dev.social.backend.services.room;

import com.dev.social.backend.enums.MemberGroupStatusEnum;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.models.UserAndRoom;

import java.util.List;


public interface UserAndRoomService {
    List<Room> getRoomsByUser(User user);

    boolean isUserInRoom(User user, Room room);

    void inviteUser(User user, Room room, boolean isCreator);

    void addUser(User user, Room room, boolean isCreator);

    Boolean isRoomCreator(User user, Room room);

    UserAndRoom getUserAndRoomByUserAndRoom(User user, Room room);

    UserAndRoom save(UserAndRoom userAndRoom);

    List<UserAndRoom> getUserAndRoomByRoom(Room room);

    boolean isUserInvited(User user, Room room);

    void removeUserFromRoom(User user, Room room);

    List<Room> getRoomsByUserAndStatusAndIsOwner(User user, MemberGroupStatusEnum status, boolean isOwner);
}
