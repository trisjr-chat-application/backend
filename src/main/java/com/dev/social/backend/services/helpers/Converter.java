package com.dev.social.backend.services.helpers;

import com.dev.social.backend.dtos.*;
import com.dev.social.backend.models.*;

public interface Converter {
    UserDto convertUserToUserDto(User user);

    MessageDto convertMessageToMessageDto(Message message);

    MessageStatusDto convertMessageStatusToMessageStatusDto(MessageStatus messageStatus);

    RoomDto convertRoomToRoomDto(Room room, User user);

    UserAndRoomDto convertUserAndRoomToUserAndRoomDto(UserAndRoom userAndRoom);
}
