package com.dev.social.backend.services.room;

import com.dev.social.backend.enums.MemberGroupStatusEnum;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.models.UserAndRoom;
import com.dev.social.backend.repositories.UserAndRoomRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Log4j2
public class UserAndRoomServiceImpl implements UserAndRoomService {
    private final UserAndRoomRepository userAndRoomRepository;

    public UserAndRoomServiceImpl(UserAndRoomRepository userAndRoomRepository) {
        this.userAndRoomRepository = userAndRoomRepository;
    }


    @Override
    public List<Room> getRoomsByUser(User user) {
        return userAndRoomRepository.findAllByUserAndStatus(user, MemberGroupStatusEnum.JOINED).stream().map(UserAndRoom::getRoom).toList();
    }

    @Override
    public boolean isUserInRoom(User user, Room room) {
        return userAndRoomRepository.existsByUserAndRoomAndStatus(user, room, MemberGroupStatusEnum.JOINED);
    }

    @Override
    public void inviteUser(User user, Room room, boolean isCreator) {
        UserAndRoom userAndRoom = UserAndRoom.builder()
                .user(user)
                .room(room)
                .isCreator(isCreator)
                .status(isCreator ? MemberGroupStatusEnum.JOINED : MemberGroupStatusEnum.INVITED)
                .build();
        userAndRoomRepository.save(userAndRoom);
    }

    @Override
    public void addUser(User user, Room room, boolean isCreator) {
        UserAndRoom userAndRoom = UserAndRoom.builder()
                .user(user)
                .room(room)
                .isCreator(isCreator)
                .status(MemberGroupStatusEnum.JOINED)
                .lastSeenAt(new Date())
                .build();
        userAndRoomRepository.save(userAndRoom);
    }

    @Override
    public Boolean isRoomCreator(User user, Room room) {
        UserAndRoom userAndRoom = userAndRoomRepository.getUserAndRoomByUserAndRoom(user, room);
        if (userAndRoom == null) return false;
        return userAndRoom.getIsCreator();
    }

    @Override
    public UserAndRoom getUserAndRoomByUserAndRoom(User user, Room room) {
        return userAndRoomRepository.getUserAndRoomByUserAndRoom(user, room);
    }

    @Override
    public UserAndRoom save(UserAndRoom userAndRoom) {
        return userAndRoomRepository.save(userAndRoom);
    }

    @Override
    public List<UserAndRoom> getUserAndRoomByRoom(Room room) {
        return userAndRoomRepository.getUserAndRoomByRoom(room);
    }

    @Override
    public boolean isUserInvited(User user, Room room) {
        return userAndRoomRepository.existsByUserAndRoom(user, room);
    }

    @Override
    @Transactional
    public void removeUserFromRoom(User user, Room room) {
        UserAndRoom userAndRoom = userAndRoomRepository.getUserAndRoomByUserAndRoom(user, room);
        if (userAndRoom == null) return;
        userAndRoomRepository.deleteUserAndRoomById(userAndRoom.getId());
    }

    @Override
    public List<Room> getRoomsByUserAndStatusAndIsOwner(User user, MemberGroupStatusEnum status, boolean isOwner) {
        return userAndRoomRepository.findAllByUserAndStatusAndIsCreator(user, status, isOwner).stream().map(UserAndRoom::getRoom).toList();
    }
}
