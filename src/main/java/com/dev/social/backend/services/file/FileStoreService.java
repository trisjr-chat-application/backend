package com.dev.social.backend.services.file;

import com.dev.social.backend.models.File;
import com.dev.social.backend.responses.FileResponse.UploadFileDataResponse;
import com.google.cloud.storage.Bucket;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStoreService {
    Bucket getBucket() throws IOException;

    UploadFileDataResponse uploadFile(MultipartFile file) throws IOException;

    String convertToBase64(byte[] file);

    String getSingedUrl(String filePath) throws IOException;

    File getFile(String filePath);
}
