package com.dev.social.backend.services.user;

import com.dev.social.backend.models.Relationship;
import com.dev.social.backend.models.User;
import com.dev.social.backend.repositories.RelationshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RelationshipServiceImpl implements RelationshipService {
    private final RelationshipRepository relationshipRepository;

    @Autowired
    public RelationshipServiceImpl(RelationshipRepository relationshipRepository) {
        this.relationshipRepository = relationshipRepository;
    }

    @Override
    public Relationship createRelationship(User sender, User receiver) {
        Relationship relationship = Relationship.builder()
                .sender(sender)
                .receiver(receiver)
                .isAccepted(false)
                .build();
        return relationshipRepository.save(relationship);
    }

    @Override
    public List<User> getFriends(User user, String key) {
        List<Relationship> senderRelationships = relationshipRepository.findAllBySenderAndIsAccepted(user, true);
        List<Relationship> receiverRelationships = relationshipRepository.findAllByReceiverAndIsAccepted(user, true);
        List<User> friends = new ArrayList<>();
        for (Relationship relationship : senderRelationships) {
            User friend = relationship.getReceiver();
            String fullName = friend.getFirstName() + " " + friend.getLastName();
            if (fullName.toLowerCase().contains(key.toLowerCase())) {
                friends.add(friend);
            }
        }
        for (Relationship relationship : receiverRelationships) {
            User friend = relationship.getSender();
            String fullName = friend.getFirstName() + " " + friend.getLastName();
            if (fullName.toLowerCase().contains(key.toLowerCase())) {
                friends.add(friend);
            }
        }
        return friends;
    }

    @Override
    public List<User> getAllSentRequest(User user) {
        List<Relationship> senderRelationships = relationshipRepository.findAllBySenderAndIsAccepted(user, false);
        List<User> friends = new ArrayList<>();
        for (Relationship relationship : senderRelationships) {
            friends.add(relationship.getReceiver());
        }
        return friends;
    }

    @Override
    public List<User> getAllReceivedRequest(User user) {
        List<Relationship> receiverRelationships = relationshipRepository.findAllByReceiverAndIsAccepted(user, false);
        List<User> friends = new ArrayList<>();
        for (Relationship relationship : receiverRelationships) {
            friends.add(relationship.getSender());
        }
        return friends;
    }

    @Override
    public Relationship getRelationship(User sender, User receiver) {
        if (relationshipRepository.findRelationshipBySenderAndReceiver(sender, receiver) == null)
            return relationshipRepository.findRelationshipBySenderAndReceiver(receiver, sender);
        return relationshipRepository.findRelationshipBySenderAndReceiver(sender, receiver);
    }

    @Override
    public void save(Relationship relationship) {
        relationshipRepository.save(relationship);
    }

    @Override
    public void delete(Relationship relationship) {
        relationshipRepository.delete(relationship);
    }

    @Override
    public Boolean isFriend(User user, User friend) {
        Relationship relationship = getRelationship(user, friend);
        return relationship != null && relationship.getIsAccepted();
    }
}
