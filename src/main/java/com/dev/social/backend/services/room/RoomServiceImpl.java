package com.dev.social.backend.services.room;

import com.dev.social.backend.enums.RoomTypeEnum;
import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.models.UserAndRoom;
import com.dev.social.backend.repositories.MessageRepository;
import com.dev.social.backend.repositories.RoomRepository;
import com.dev.social.backend.services.file.FileStoreService;
import com.dev.social.backend.services.helpers.Converter;
import com.dev.social.backend.services.message.MessageStatusService;
import com.dev.social.backend.types.ChatItem;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class RoomServiceImpl implements RoomService {
    private final RoomRepository roomRepository;
    private final UserAndRoomService userAndRoomService;
    private final Converter converter;
    private final MessageStatusService messageStatusService;
    private final FileStoreService fileStoreService;
    private final MessageRepository messageRepository;

    @Override
    public Room getRoomByKey(String key) {
        return roomRepository.findRoomByRoomKey(key);
    }

    @Override
    public Room createRoom(String name, String key, String photo, RoomTypeEnum type) {
        Room room = Room.builder()
                .name(name)
                .roomKey(key)
                .image(photo)
                .type(type)
                .build();
        return roomRepository.save(room);
    }

    @Override
    public Room save(Room room) {
        return roomRepository.save(room);
    }

    @Override
    public Room createPrivateRoom(List<User> users) {
        String roomKey = String.valueOf(System.currentTimeMillis());
        Room room = Room.builder()
                .roomKey(roomKey)
                .type(RoomTypeEnum.PRIVATE)
                .build();
        Room saved = roomRepository.save(room);

        users.forEach(user -> userAndRoomService.addUser(user, saved, false));
        return saved;
    }

    @SneakyThrows
    @Override
    public ChatItem getChatItem(User user, Room room) {
        if (room == null) return null;
        String photo = room.getImage();
        Message lastMessage = messageRepository.findFirstByRoomOrderByCreatedAtDesc(room);

        Date lastSeen = userAndRoomService.getUserAndRoomByUserAndRoom(user, room).getLastSeenAt();
        Date messageCreatedAt = lastMessage.getCreatedAt();
        Boolean isSeen = lastSeen != null && lastSeen.compareTo(messageCreatedAt) >= 0;

        ChatItem chatItem = ChatItem.builder()
                .lastMessage(converter.convertMessageToMessageDto(lastMessage))
                .key(room.getRoomKey())
                .name(room.getName())
                .messageStatus(converter.convertMessageStatusToMessageStatusDto(messageStatusService.getMessageStatusByUserAndMessage(user, lastMessage)))
                .isSeen(isSeen)
                .build();
        if (room.getType() == RoomTypeEnum.PRIVATE) {
            List<User> users = room.getUsersInvited().stream().map(UserAndRoom::getUser).toList();
            for (User u : users) {
                if (!u.getId().equals(user.getId())) {
                    String name = u.getFirstName() + " " + u.getLastName();
                    chatItem.setName(name);
                    // Check profilePicture is link or not
                    if (u.getProfilePicture() != null && !u.getProfilePicture().isEmpty() && !u.getProfilePicture().contains("http"))
                        chatItem.setPhoto(fileStoreService.getFile(u.getProfilePicture()).getUrl());
                    else
                        chatItem.setPhoto(u.getProfilePicture());
                }
            }
        } else {
            if (photo != null && !photo.isEmpty() && !photo.contains("http"))
                chatItem.setPhoto(fileStoreService.getFile(photo).getUrl());
            else
                chatItem.setPhoto(photo);
        }

        return chatItem;
    }
}
