package com.dev.social.backend.services.role;

import com.dev.social.backend.enums.RoleCodeEnum;
import com.dev.social.backend.models.Role;
import com.dev.social.backend.repositories.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Override
    public Role getByRoleCode(RoleCodeEnum role) {
        return roleRepository.findByRoleCode(role);
    }

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
}
