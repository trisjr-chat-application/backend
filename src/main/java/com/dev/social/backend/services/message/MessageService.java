package com.dev.social.backend.services.message;

import com.dev.social.backend.enums.MessageEnum;
import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.outputs.CreateMessageOutput;
import com.dev.social.backend.requests.MessageRequest.SendMessageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MessageService {
    CreateMessageOutput createMessage(User sender, SendMessageRequest request);

    List<Message> getAllMessagesToRoom(Room recipient, Pageable pageable);

    Message save(Message message);

    Message createMessageNotificationToRoom(Room room, String content);

    Message getMessageById(Long id);

    List<Message> getAllMessagesToRoomWithType(Room room, MessageEnum type);
}
