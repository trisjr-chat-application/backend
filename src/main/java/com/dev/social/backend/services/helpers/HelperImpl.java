package com.dev.social.backend.services.helpers;

import com.dev.social.backend.types.UrlPreview;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class HelperImpl implements Helper {
    private static final String URL_PATTERN =
            "(?:\\S+(?::\\S*)?@)?(?:(?!(?:10|127)(?:\\.\\d{1,3}){3})(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-5])|(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*\\.[a-z\\u00a1-\\uffff]{2,})(?::\\d{2,5})?(?:[/?#]\\S*)?";

    @Override
    public UrlPreview getLinkPreview(String text) throws IOException {
        String url = getUrlFromText(text);
        if (url == null) return null;

        Document doc = Jsoup.connect(url).get();
        String title = doc.title();
        String description = doc.select("meta[property=og:description]").attr("content");

        String image = Objects.requireNonNull(doc.select("img[src~=(?i)\\.(png|jpe?g|gif)]").first()).absUrl("src");
        if (!isUrl(image)) {
            image = "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg";
        }
        return UrlPreview.builder()
                .title(title)
                .description(description)
                .image(image)
                .url(url)
                .build();
    }

    @Override
    public String getUrlFromText(String text) {
        Pattern pattern = Pattern.compile(URL_PATTERN);
        Matcher matcher = pattern.matcher(text);

        if (matcher.find()) {
            String url = matcher.group();
            if (!url.startsWith("http://") && !url.startsWith("https://"))
                url = "http://" + url;
            return url;
        }
        return null;
    }

    public boolean isUrl(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            return false;

        Pattern pattern = Pattern.compile(URL_PATTERN);
        Matcher matcher = pattern.matcher(url);
        return matcher.find();
    }
}
