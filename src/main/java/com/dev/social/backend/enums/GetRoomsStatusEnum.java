package com.dev.social.backend.enums;

public enum GetRoomsStatusEnum {
    JOINED,
    INVITED,
    OWNER,
}
