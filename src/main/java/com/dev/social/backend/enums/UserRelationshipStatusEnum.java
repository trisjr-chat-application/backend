package com.dev.social.backend.enums;

public enum UserRelationshipStatusEnum {
    FRIEND,
    SENT_REQUEST,
    RECEIVED_REQUEST,
    NONE
}
