package com.dev.social.backend.enums;

public enum RoleCodeEnum {
    ROLE_USER,
    ROLE_ADMIN
}
