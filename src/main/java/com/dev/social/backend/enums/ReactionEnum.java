package com.dev.social.backend.enums;

public enum ReactionEnum {
    NONE,
    LIKE,
    HAHA,
    WOW,
    SAD,
    ANGRY,
    LOVE
}
