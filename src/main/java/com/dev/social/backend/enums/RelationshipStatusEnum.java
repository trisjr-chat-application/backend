package com.dev.social.backend.enums;

public enum RelationshipStatusEnum {
    ENGAGED,
    MARRIED,
    SINGLE,
    DIVORCED,
    WIDOWED,
    NONE
}
