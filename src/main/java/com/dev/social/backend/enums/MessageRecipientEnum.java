package com.dev.social.backend.enums;

public enum MessageRecipientEnum {
    ROOM,
    USER
}
