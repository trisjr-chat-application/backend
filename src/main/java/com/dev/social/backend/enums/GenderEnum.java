package com.dev.social.backend.enums;

public enum GenderEnum {
    MALE,
    FEMALE,
    DIFF
}
