package com.dev.social.backend.enums;

public enum MemberGroupStatusEnum {
    INVITED,
    JOINED
}
