package com.dev.social.backend.enums;

public enum NotificationStatusEnum {
    SUCCESS,
    ERROR,
    INFO,
    WARNING,
}
