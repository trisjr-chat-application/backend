package com.dev.social.backend.enums;

public enum FileEnum {
    IMAGE,
    VIDEO,
    AUDIO,
    DOCUMENT,
    OTHER
}
