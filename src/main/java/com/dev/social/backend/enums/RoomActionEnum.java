package com.dev.social.backend.enums;

public enum RoomActionEnum {
    ACCEPT,
    REJECT,
    LEAVE,
    ADD,
    REMOVE,
    DELETE,
}
