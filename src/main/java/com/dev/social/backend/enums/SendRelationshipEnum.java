package com.dev.social.backend.enums;

public enum SendRelationshipEnum {
    ADD_FRIEND,
    ACCEPT_REQUEST,
    REJECT_REQUEST,
    CANCEL_REQUEST,
    UNFRIEND
}
