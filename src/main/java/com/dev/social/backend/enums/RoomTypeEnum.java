package com.dev.social.backend.enums;

public enum RoomTypeEnum {
    PRIVATE,
    GROUP
}
