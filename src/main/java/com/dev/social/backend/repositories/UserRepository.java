package com.dev.social.backend.repositories;

import com.dev.social.backend.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    User findByPhoneNumber(String phoneNumber);

    User findUserById(Long id);
}
