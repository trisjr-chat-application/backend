package com.dev.social.backend.repositories;

import com.dev.social.backend.models.Relationship;
import com.dev.social.backend.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RelationshipRepository extends JpaRepository<Relationship, Long> {
    Relationship findRelationshipBySenderAndReceiver(User sender, User receiver);

    List<Relationship> findAllBySenderAndIsAccepted(User sender, boolean isAccepted);

    List<Relationship> findAllByReceiverAndIsAccepted(User receiver, boolean isAccepted);
}