package com.dev.social.backend.repositories;

import com.dev.social.backend.enums.MessageEnum;
import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.Room;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long>, PagingAndSortingRepository<Message, Long> {

    List<Message> findAllByRoom(Room roomRecipient, Pageable pageable);

    Message findFirstByRoomOrderByCreatedAtDesc(Room room);

    List<Message> findAllByRoomAndType(Room room, MessageEnum type);
}