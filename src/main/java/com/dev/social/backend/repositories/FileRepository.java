package com.dev.social.backend.repositories;

import com.dev.social.backend.models.File;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<File, Long> {

    File findByPath(String path);

    Boolean existsByPath(String path);
}