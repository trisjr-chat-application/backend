package com.dev.social.backend.repositories;

import com.dev.social.backend.enums.RoleCodeEnum;
import com.dev.social.backend.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRoleCode(RoleCodeEnum roleCode);
}