package com.dev.social.backend.repositories;

import com.dev.social.backend.enums.MemberGroupStatusEnum;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.models.UserAndRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserAndRoomRepository extends JpaRepository<UserAndRoom, Long> {
    List<UserAndRoom> findAllByUserAndStatus(User user, MemberGroupStatusEnum status);

    List<UserAndRoom> findAllByUserAndStatusAndIsCreator(User user, MemberGroupStatusEnum status, boolean isCreator);

    Boolean existsByUserAndRoomAndStatus(User user, Room room, MemberGroupStatusEnum status);

    UserAndRoom getUserAndRoomByUserAndRoom(User user, Room room);

    List<UserAndRoom> getUserAndRoomByRoom(Room room);

    Boolean existsByUserAndRoom(User user, Room room);

    @Modifying
    @Query("DELETE FROM UserAndRoom uar WHERE uar.id = ?1")
    void deleteUserAndRoomById(Long id);
}