package com.dev.social.backend.repositories;

import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.MessageStatus;
import com.dev.social.backend.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageStatusRepository extends JpaRepository<MessageStatus, Long> {
    MessageStatus findByUserAndMessage(User user, Message message);
}