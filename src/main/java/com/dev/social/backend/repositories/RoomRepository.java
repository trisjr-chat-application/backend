package com.dev.social.backend.repositories;

import com.dev.social.backend.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
    Room findRoomByRoomKey(String roomKey);

    Room findRoomById(Long id);
}