package com.dev.social.backend.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {
    private String email;
    private String password;
    private Boolean remember;

    public Boolean isEmpty() {
        return email.isEmpty() || password.isEmpty();
    }
}
