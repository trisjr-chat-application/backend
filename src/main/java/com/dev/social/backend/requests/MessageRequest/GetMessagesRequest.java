package com.dev.social.backend.requests.MessageRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetMessagesRequest {
    private String key;
    private Integer page;
    private Integer perPage = 25;
}
