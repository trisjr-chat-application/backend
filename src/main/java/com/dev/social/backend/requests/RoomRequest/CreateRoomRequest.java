package com.dev.social.backend.requests.RoomRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateRoomRequest {
    private String name;
    private String key;
    private String photo;
    private List<Long> members;
}
