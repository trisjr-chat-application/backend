package com.dev.social.backend.requests.UserRequest;

import com.dev.social.backend.enums.SendRelationshipEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendRelationshipRequest {
    private Long userId;
    private SendRelationshipEnum type;
}
