package com.dev.social.backend.requests.MessageRequest;

import com.dev.social.backend.enums.ReactionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReactionMessageRequest {
    private Long messageId;
    private ReactionEnum reaction;
}
