package com.dev.social.backend.requests.RoomRequest;

import com.dev.social.backend.enums.RoomActionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomActionRequest {
    private String key;
    private RoomActionEnum action;
    private List<Long> usersId;
}
