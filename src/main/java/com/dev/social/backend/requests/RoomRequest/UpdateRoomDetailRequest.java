package com.dev.social.backend.requests.RoomRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateRoomDetailRequest {
    private String key;
    private String name;
    private String image;
}
