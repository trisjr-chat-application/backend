package com.dev.social.backend.requests.MessageRequest;

import com.dev.social.backend.enums.MessageEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendMessageRequest {
    private String key;
    private String content;
    private MessageEnum type;
    private Long replyMessageId;
}
