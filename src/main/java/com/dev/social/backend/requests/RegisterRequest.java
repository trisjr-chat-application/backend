package com.dev.social.backend.requests;

import com.dev.social.backend.enums.GenderEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Date birthDate;
    private String password;
    private String profilePicture;
    @Enumerated(EnumType.STRING)
    private GenderEnum gender;

    public Boolean isEmpty() {
        return firstName == null || lastName == null || email == null || phoneNumber == null || birthDate == null || password == null;
    }
}
