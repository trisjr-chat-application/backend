package com.dev.social.backend.requests.FileRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


// Request from antdv a-upload component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConvertToBase64Request {
    private Object file;

    private String filename;

}
