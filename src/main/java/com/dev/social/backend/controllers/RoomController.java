package com.dev.social.backend.controllers;


import com.dev.social.backend.configs.jwt.TokenManager;
import com.dev.social.backend.dtos.MessageDto;
import com.dev.social.backend.dtos.RoomDto;
import com.dev.social.backend.dtos.UserDto;
import com.dev.social.backend.enums.*;
import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.models.UserAndRoom;
import com.dev.social.backend.requests.RoomRequest.CreateRoomRequest;
import com.dev.social.backend.requests.RoomRequest.HandleRoomActionRequest;
import com.dev.social.backend.requests.RoomRequest.UpdateRoomDetailRequest;
import com.dev.social.backend.responses.Response;
import com.dev.social.backend.responses.RoomResponse.CheckRoomKeyResponse;
import com.dev.social.backend.responses.RoomResponse.GetRoomDetailResponse;
import com.dev.social.backend.responses.RoomResponse.GetRoomInvitedResponse;
import com.dev.social.backend.responses.RoomResponse.GetUsersInviteResponse;
import com.dev.social.backend.services.helpers.Converter;
import com.dev.social.backend.services.message.MessageService;
import com.dev.social.backend.services.room.RoomService;
import com.dev.social.backend.services.room.UserAndRoomService;
import com.dev.social.backend.services.user.RelationshipService;
import com.dev.social.backend.services.user.UserService;
import com.dev.social.backend.types.Notification;
import com.dev.social.backend.types.SendMessage;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping("/api/room")
public class RoomController {
    private final RoomService roomService;
    private final TokenManager tokenManager;
    private final UserService userService;
    private final UserAndRoomService userAndRoomService;
    private final RelationshipService relationshipService;
    private final MessageService messageService;
    private final Converter converter;
    private final SimpMessagingTemplate messagingTemplate;

    public RoomController(RoomService roomService, TokenManager tokenManager, UserService userService, UserAndRoomService userAndRoomService, RelationshipService relationshipService, MessageService messageService, Converter converter, SimpMessagingTemplate messagingTemplate) {
        this.roomService = roomService;
        this.tokenManager = tokenManager;
        this.userService = userService;
        this.userAndRoomService = userAndRoomService;
        this.relationshipService = relationshipService;
        this.messageService = messageService;
        this.converter = converter;
        this.messagingTemplate = messagingTemplate;
    }

    @PostMapping(value = "/create-room", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> createRoom(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestBody CreateRoomRequest request) {
        String token = authorizationHeader.substring(7);
        String email = tokenManager.getUsernameFromToken(token);
        User user = userService.getUserByEmail(email);

        if (user == null) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("User not found")
                            .build()
            );
        }

        Room room = roomService.createRoom(request.getName(), request.getKey(), request.getPhoto(), RoomTypeEnum.GROUP);
        userAndRoomService.inviteUser(user, room, true);

        List<Long> members = request.getMembers();
        for (Long id : members) {
            User member = userService.getUserById(id);
            if (member != null) {
                if (!userAndRoomService.isUserInRoom(member, room)) {
                    userAndRoomService.inviteUser(member, room, false);

                    Notification notification = Notification.builder()
                            .status(NotificationStatusEnum.INFO)
                            .content("You were invited to room " + room.getName())
                            .build();
                    messagingTemplate.convertAndSendToUser(member.getId().toString(), "/queue/notifications", notification);
                }
            }
        }

        messageService.createMessageNotificationToRoom(room, "Room created by " + user.getFirstName() + " " + user.getLastName());

        return ResponseEntity.ok(
                Response.builder()
                        .status(true)
                        .message("Room created")
                        .build()
        );
    }

    @GetMapping(value = "/check-key", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> checkKey(@RequestParam String key) {
        Room room = roomService.getRoomByKey(key);
        boolean isValid = key != null && room == null && key.matches(".*[a-zA-Z]+.*");
        CheckRoomKeyResponse response = CheckRoomKeyResponse.builder()
                .isRoomKeyValid(isValid)
                .build();

        return ResponseEntity.ok(
                Response.builder()
                        .status(true)
                        .message("Room key checked")
                        .data(response)
                        .build()
        );
    }

    @GetMapping(value = "/get-room-detail", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getRoomDetail(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestParam String key) {
        String token = authorizationHeader.substring(7);
        String email = tokenManager.getUsernameFromToken(token);
        User user = userService.getUserByEmail(email);

        Room room = roomService.getRoomByKey(key);
        if (room == null) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("Room not found")
                            .build()
            );
        }

        if (!userAndRoomService.isUserInRoom(user, room)) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("You are not in this room")
                            .build()
            );
        }

        RoomDto roomDto = converter.convertRoomToRoomDto(room, user);
        List<MessageDto> imageMessages = messageService.getAllMessagesToRoomWithType(room, MessageEnum.IMAGE)
                .stream().map(converter::convertMessageToMessageDto).toList();
        GetRoomDetailResponse response = GetRoomDetailResponse.builder()
                .room(roomDto)
                .isCreator(userAndRoomService.isRoomCreator(user, room))
                .imageMessages(imageMessages)
                .build();

        return ResponseEntity.ok(
                Response.builder()
                        .status(true)
                        .message("Get room detail success")
                        .data(response)
                        .build()
        );
    }

    @PostMapping(value = "/update-room-detail", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> updateRoomDetail(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestBody UpdateRoomDetailRequest request) {
        String token = authorizationHeader.substring(7);
        String email = tokenManager.getUsernameFromToken(token);
        User user = userService.getUserByEmail(email);

        Room room = roomService.getRoomByKey(request.getKey());
        if (room == null) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("Room not found")
                            .build()
            );
        }

        if (!userAndRoomService.isRoomCreator(user, room)) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("You are not creator of this room")
                            .build()
            );
        }

        if (request.getName() != null && !request.getName().isEmpty()) {
            room.setName(request.getName());
        }
        if (request.getImage() != null && !request.getImage().isEmpty()) {
            room.setImage(request.getImage());
        }
        roomService.save(room);

        if (request.getName() != null && !request.getName().isEmpty() || request.getImage() != null && !request.getImage().isEmpty()) {
            Message message = messageService.createMessageNotificationToRoom(room, "Room detail was updated by " + user.getLastName());

            List<UserAndRoom> userAndRooms = userAndRoomService.getUserAndRoomByRoom(room);
            List<User> users = userAndRooms.stream().map(UserAndRoom::getUser).toList();

            users.forEach(u -> {
                if (!u.equals(user)) {
                    SendMessage sendMessage = SendMessage.builder()
                            .message(converter.convertMessageToMessageDto(message))
                            .room(converter.convertRoomToRoomDto(room, u))
                            .build();
                    messagingTemplate.convertAndSendToUser(user.getId().toString(), "/queue/messages", sendMessage);
                }
            });
        }

        return ResponseEntity.ok(
                Response.builder()
                        .status(true)
                        .message("Update room detail success")
                        .build()
        );
    }

    @GetMapping(value = "/get-users-invite", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getUsersInvite(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestParam String key, @RequestParam String search) {
        User user = userService.getUserByAuthorizationHeader(authorizationHeader);

        Room room = roomService.getRoomByKey(key);
        if (room == null) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("Room not found")
                            .build()
            );
        }

        if (!userAndRoomService.isRoomCreator(user, room)) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("You are not creator of this room")
                            .build()
            );
        }

        List<User> users = relationshipService.getFriends(user, search);
        List<User> usersInvite = new ArrayList<>();
        for (User u : users) {
            if (!userAndRoomService.isUserInvited(u, room)) {
                usersInvite.add(u);
            }
        }

        List<UserDto> usersInviteList = usersInvite.stream().map(converter::convertUserToUserDto).toList();
        GetUsersInviteResponse response = GetUsersInviteResponse.builder()
                .users(usersInviteList)
                .build();
        return ResponseEntity.ok(
                Response.builder()
                        .status(true)
                        .message("Get users invite success")
                        .data(response)
                        .build()
        );
    }

    @PostMapping(value = "/handle-room-action", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> handleRoomAction(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestBody HandleRoomActionRequest request) {
        User user = userService.getUserByAuthorizationHeader(authorizationHeader);
        if (user == null) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("User not found")
                            .build()
            );
        }
        Room room = roomService.getRoomByKey(request.getKey());
        if (room == null) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("Room not found")
                            .build()
            );
        }
        List<User> usersInRoom = userAndRoomService.getUserAndRoomByRoom(room).stream().map(UserAndRoom::getUser).toList();
        RoomActionEnum action = request.getAction();
        List<User> users = new ArrayList<>();
        if (request.getUsersId() != null) {
            for (Long id : request.getUsersId()) {
                User u = userService.getUserById(id);
                if (u != null) {
                    users.add(u);
                }
            }
        }
        switch (action) {
            case ADD -> {
                List<User> usersAdded = new ArrayList<>();
                if (users.isEmpty()) {
                    return ResponseEntity.badRequest().body(
                            Response.builder()
                                    .status(false)
                                    .message("Users not found")
                                    .build()
                    );
                }
                users.forEach(u -> {
                    if (!userAndRoomService.isUserInRoom(u, room)) {
                        userAndRoomService.inviteUser(u, room, false);
                        usersAdded.add(u);

                        // send notification to user
                        Notification notification = Notification.builder()
                                .status(NotificationStatusEnum.INFO)
                                .content("You were invited to room " + room.getName())
                                .build();
                        messagingTemplate.convertAndSendToUser(u.getId().toString(), "/queue/notifications", notification);
                    }
                });
                String messageContent;
                if (usersAdded.size() <= 3) {
                    messageContent = usersAdded.stream().map(User::getLastName).collect(Collectors.joining(", ")) + " was invited to room";
                } else {
                    messageContent = usersAdded.get(0).getLastName() + " and " + (usersAdded.size() - 1) + " others were invited to room";
                }
                Message message = messageService.createMessageNotificationToRoom(room, messageContent);
                // send notification to users in room
                SendMessage sendMessage = SendMessage.builder()
                        .message(converter.convertMessageToMessageDto(message))
                        .room(converter.convertRoomToRoomDto(room, user))
                        .build();
                usersInRoom.forEach(userInRoom -> messagingTemplate.convertAndSendToUser(userInRoom.getId().toString(), "/queue/messages", sendMessage));
            }

            case REMOVE -> {
                List<User> usersRemoved = new ArrayList<>();
                if (users.isEmpty()) {
                    return ResponseEntity.badRequest().body(
                            Response.builder()
                                    .status(false)
                                    .message("Users not found")
                                    .build()
                    );
                }
                users.forEach(u -> {
                    if (userAndRoomService.isUserInvited(u, room)) {
                        userAndRoomService.removeUserFromRoom(u, room);
                        usersRemoved.add(u);

                        // send notification to user
                        Notification notification = Notification.builder()
                                .status(NotificationStatusEnum.ERROR)
                                .content("You were removed from room " + room.getName())
                                .build();
                        messagingTemplate.convertAndSendToUser(u.getId().toString(), "/queue/notifications", notification);
                    }
                });

                if (usersRemoved.size() != 0) {
                    String messageContent;
                    if (usersRemoved.size() <= 3) {
                        messageContent = usersRemoved.stream().map(User::getLastName).collect(Collectors.joining(", ")) + " was removed from room";
                    } else {
                        messageContent = usersRemoved.get(0).getLastName() + " and " + (usersRemoved.size() - 1) + " others were removed from room";
                    }
                    Message message = messageService.createMessageNotificationToRoom(room, messageContent);
                    // send notification to users in room
                    SendMessage sendMessage = SendMessage.builder()
                            .message(converter.convertMessageToMessageDto(message))
                            .room(converter.convertRoomToRoomDto(room, user))
                            .build();
                    usersInRoom.forEach(userInRoom -> messagingTemplate.convertAndSendToUser(userInRoom.getId().toString(), "/queue/messages", sendMessage));
                }
            }

            case ACCEPT -> {
                if (!userAndRoomService.isUserInvited(user, room)) {
                    return ResponseEntity.badRequest().body(
                            Response.builder()
                                    .status(false)
                                    .message("You are not invited to this room")
                                    .build()
                    );
                }
                UserAndRoom userAndRoom = userAndRoomService.getUserAndRoomByUserAndRoom(user, room);
                userAndRoom.setStatus(MemberGroupStatusEnum.JOINED);
                userAndRoom.setLastSeenAt(new Date());
                userAndRoomService.save(userAndRoom);

                Message message = messageService.createMessageNotificationToRoom(room, user.getLastName() + " joined room");

                // send notification to users in room
                SendMessage sendMessage = SendMessage.builder()
                        .message(converter.convertMessageToMessageDto(message))
                        .room(converter.convertRoomToRoomDto(room, user))
                        .build();
                usersInRoom.forEach(userInRoom -> messagingTemplate.convertAndSendToUser(userInRoom.getId().toString(), "/queue/messages", sendMessage));

                // send notification to user
                Notification notification = Notification.builder()
                        .status(NotificationStatusEnum.SUCCESS)
                        .content("You joined room " + room.getName())
                        .build();
                messagingTemplate.convertAndSendToUser(user.getId().toString(), "/queue/notifications", notification);
            }

            case REJECT -> {
                if (!userAndRoomService.isUserInvited(user, room)) {
                    return ResponseEntity.badRequest().body(
                            Response.builder()
                                    .status(false)
                                    .message("You are not invited to this room")
                                    .build()
                    );
                }
                userAndRoomService.removeUserFromRoom(user, room);

                Message message = messageService.createMessageNotificationToRoom(room, user.getLastName() + " rejected room");

                // send messages to users in room
                SendMessage sendMessage = SendMessage.builder()
                        .message(converter.convertMessageToMessageDto(message))
                        .room(converter.convertRoomToRoomDto(room, user))
                        .build();
                usersInRoom.forEach(userInRoom -> messagingTemplate.convertAndSendToUser(userInRoom.getId().toString(), "/queue/messages", sendMessage));

                // send notification to user
                Notification notification = Notification.builder()
                        .status(NotificationStatusEnum.WARNING)
                        .content("You rejected room " + room.getName())
                        .build();
                messagingTemplate.convertAndSendToUser(user.getId().toString(), "/queue/notifications", notification);
            }

            case LEAVE -> {
                if (!userAndRoomService.isUserInRoom(user, room)) {
                    return ResponseEntity.badRequest().body(
                            Response.builder()
                                    .status(false)
                                    .message("You are not in this room")
                                    .build()
                    );
                }
                userAndRoomService.removeUserFromRoom(user, room);

                Message message = messageService.createMessageNotificationToRoom(room, user.getLastName() + " left room");

                // send notification to users in room
                SendMessage sendMessage = SendMessage.builder()
                        .message(converter.convertMessageToMessageDto(message))
                        .room(converter.convertRoomToRoomDto(room, user))
                        .build();
                usersInRoom.forEach(userInRoom -> messagingTemplate.convertAndSendToUser(userInRoom.getId().toString(), "/queue/messages", sendMessage));

                // send notification to user
                Notification notification = Notification.builder()
                        .status(NotificationStatusEnum.WARNING)
                        .content("You left room " + room.getName())
                        .build();
                messagingTemplate.convertAndSendToUser(user.getId().toString(), "/queue/notifications", notification);
            }
            case DELETE -> {
                // TODO: delete room
            }

            default -> {
                return ResponseEntity.badRequest().body(
                        Response.builder()
                                .status(false)
                                .message("Action not found")
                                .build()
                );
            }
        }

        return ResponseEntity.ok(
                Response.builder()
                        .status(true)
                        .message("Handle room action success")
                        .build()
        );
    }

    @GetMapping(value = "/get-rooms", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getRooms(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, String search, GetRoomsStatusEnum status) {
        User user = userService.getUserByAuthorizationHeader(authorizationHeader);
        if (user == null) {
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .status(false)
                            .message("User not found")
                            .build()
            );
        }

        List<Room> rooms;

        switch (status) {
            case OWNER ->
                    rooms = userAndRoomService.getRoomsByUserAndStatusAndIsOwner(user, MemberGroupStatusEnum.JOINED, true);
            case INVITED ->
                    rooms = userAndRoomService.getRoomsByUserAndStatusAndIsOwner(user, MemberGroupStatusEnum.INVITED, false);
            case JOINED ->
                    rooms = userAndRoomService.getRoomsByUserAndStatusAndIsOwner(user, MemberGroupStatusEnum.JOINED, false);
            default -> {
                return ResponseEntity.badRequest().body(
                        Response.builder()
                                .status(false)
                                .message("Status not found")
                                .build()
                );
            }
        }

        rooms = rooms.stream().filter(room -> room.getType() == RoomTypeEnum.GROUP).toList();

        if (search != null) {
            rooms = rooms.stream().filter(room -> room.getName().toLowerCase().contains(search.toLowerCase())).toList();
        }

        List<RoomDto> roomsDto = rooms.stream().map(room -> converter.convertRoomToRoomDto(room, user)).toList();
        GetRoomInvitedResponse response = GetRoomInvitedResponse.builder()
                .rooms(roomsDto)
                .build();

        return ResponseEntity.ok(
                Response.builder()
                        .status(true)
                        .message("Get rooms success")
                        .data(response)
                        .build()
        );
    }
}
