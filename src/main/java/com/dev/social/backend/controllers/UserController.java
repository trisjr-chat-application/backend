package com.dev.social.backend.controllers;

import com.dev.social.backend.configs.jwt.TokenManager;
import com.dev.social.backend.dtos.UserDto;
import com.dev.social.backend.enums.NotificationStatusEnum;
import com.dev.social.backend.enums.SendRelationshipEnum;
import com.dev.social.backend.enums.UserRelationshipStatusEnum;
import com.dev.social.backend.models.Relationship;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.requests.UserRequest.SendRelationshipRequest;
import com.dev.social.backend.responses.GetUsersResponse;
import com.dev.social.backend.responses.Response;
import com.dev.social.backend.services.helpers.Converter;
import com.dev.social.backend.services.message.MessageService;
import com.dev.social.backend.services.notification.NotificationService;
import com.dev.social.backend.services.room.RoomService;
import com.dev.social.backend.services.user.RelationshipService;
import com.dev.social.backend.services.user.UserService;
import com.dev.social.backend.types.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final UserService userService;
    private final RelationshipService relationshipService;
    private final TokenManager tokenManager;
    private final MessageService messageService;
    private final Converter converter;
    private final SimpUserRegistry simpUserRegistry;
    private final SimpMessagingTemplate messagingTemplate;
    private final RoomService roomService;
    private final NotificationService notificationService;

    @Autowired
    public UserController(UserService userService, RelationshipService relationshipService, TokenManager tokenManager, MessageService messageService, Converter converter, SimpUserRegistry simpUserRegistry, SimpMessagingTemplate messagingTemplate, RoomService roomService, NotificationService notificationService) {
        this.userService = userService;
        this.relationshipService = relationshipService;
        this.tokenManager = tokenManager;
        this.messageService = messageService;
        this.converter = converter;
        this.simpUserRegistry = simpUserRegistry;
        this.messagingTemplate = messagingTemplate;
        this.roomService = roomService;
        this.notificationService = notificationService;
    }

    public String getEmailFromToken(String authorizationHeader) {
        String token = authorizationHeader.substring(7);
        return tokenManager.getUsernameFromToken(token);
    }

    @GetMapping(value = "/get-users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getUsers(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestParam String key, @RequestParam UserRelationshipStatusEnum relationshipStatus) {
        String email = getEmailFromToken(authorizationHeader);
        User user = userService.getUserByEmail(email);
        if (user == null) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("User not found!")
                    .status(false)
                    .build());
        }
        List<UserDto> result = new ArrayList<>();
        if (relationshipStatus == UserRelationshipStatusEnum.NONE) {
            List<User> users = userService.getAllUsers();
            for (User u : users) {
                if (!u.getEmail().equals(email) && relationshipService.getRelationship(user, u) == null) {
                    result.add(converter.convertUserToUserDto(u));
                }
            }
        } else if (relationshipStatus == UserRelationshipStatusEnum.FRIEND) {
            List<UserDto> friends = relationshipService.getFriends(user, "")
                    .stream().map(converter::convertUserToUserDto).toList();
            result.addAll(friends);
        } else if (relationshipStatus == UserRelationshipStatusEnum.SENT_REQUEST) {
            List<UserDto> sentRequests = relationshipService.getAllSentRequest(user)
                    .stream().map(converter::convertUserToUserDto).toList();
            result.addAll(sentRequests);
        } else if (relationshipStatus == UserRelationshipStatusEnum.RECEIVED_REQUEST) {
            List<UserDto> receivedRequests = relationshipService.getAllReceivedRequest(user)
                    .stream().map(converter::convertUserToUserDto).toList();
            result.addAll(receivedRequests);
        }

        if (key != null && !key.isEmpty()) {
            List<UserDto> temp = new ArrayList<>();
            for (UserDto u : result) {
                String uFullName = u.getFirstName() + " " + u.getLastName();
                String uEmail = u.getEmail();
                String uPhoneNumber = u.getPhoneNumber();
                if (uFullName.toLowerCase().contains(key.toLowerCase()) || uEmail.toLowerCase().contains(key.toLowerCase()) || uPhoneNumber.toLowerCase().contains(key.toLowerCase())) {
                    temp.add(u);
                }
            }
            result = temp;
        }

        return ResponseEntity.ok(Response.builder()
                .message("Get users successfully!")
                .status(true)
                .data(GetUsersResponse.builder().users(result).build())
                .build());
    }

    @PostMapping(value = "send-request", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> sendRequest(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestBody SendRelationshipRequest request) {
        User sender = userService.getUserByEmail(getEmailFromToken(authorizationHeader));
        User receiver = userService.getUserById(request.getUserId());
        if (sender == null || receiver == null) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("User not found!")
                    .status(false)
                    .build());
        }
        if (sender.equals(receiver)) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("You can't send request to your self!")
                    .status(false)
                    .build());
        }
        SendRelationshipEnum type = request.getType();
        Relationship relationship = relationshipService.getRelationship(sender, receiver);
        switch (type) {
            // Add friend request
            case ADD_FRIEND -> {
                // Check if sender has already sent request to receiver
                if (relationship != null) {
                    return ResponseEntity.badRequest().body(Response.builder()
                            .message("You have already sent request to this user!")
                            .status(false)
                            .build());
                }
                relationshipService.createRelationship(sender, receiver);
                // Send notification to receiver
                String notificationContent = sender.getLastName() + " added you as friend!";
                Notification notification = notificationService.createNotification(notificationContent, NotificationStatusEnum.INFO);
                messagingTemplate.convertAndSendToUser(receiver.getId().toString(), "/queue/notifications", notification);

                return ResponseEntity.ok(Response.builder()
                        .message("Send request successfully! Please wait for response!")
                        .status(true)
                        .build());
            }
            // Accept request
            case ACCEPT_REQUEST -> {
                // Check if sender has already sent request to receiver
                if (relationship == null) {
                    return ResponseEntity.badRequest().body(Response.builder()
                            .message("You haven't received request from this user!")
                            .status(false)
                            .build());
                }
                relationship.setIsAccepted(true);
                relationshipService.save(relationship);

                // Create private room
                List<User> users = new ArrayList<>();
                users.add(sender);
                users.add(receiver);
                Room room = roomService.createPrivateRoom(users);

                // Create message notification
                messageService.createMessageNotificationToRoom(room, "You are now friends!");

                // Send notification to receiver
                String notificationContent = sender.getLastName() + " accepted your friend request!";
                Notification notification = notificationService.createNotification(notificationContent, NotificationStatusEnum.SUCCESS);
                messagingTemplate.convertAndSendToUser(receiver.getId().toString(), "/queue/notifications", notification);

                return ResponseEntity.ok(Response.builder()
                        .message("Accept request successfully!")
                        .status(true)
                        .build());
            }
            // Reject request
            case REJECT_REQUEST -> {
                // Check if sender has already sent request to receiver
                if (relationship == null) {
                    return ResponseEntity.badRequest().body(Response.builder()
                            .message("You haven't received request from this user!")
                            .status(false)
                            .build());
                }
                relationshipService.delete(relationship);

                // Send notification to receiver
                String notificationContent = sender.getLastName() + " rejected your friend request!";
                Notification notification = notificationService.createNotification(notificationContent, NotificationStatusEnum.WARNING);
                messagingTemplate.convertAndSendToUser(receiver.getId().toString(), "/queue/notifications", notification);

                return ResponseEntity.ok(Response.builder()
                        .message("Reject request successfully!")
                        .status(true)
                        .build());
            }
            // Cancel request
            case CANCEL_REQUEST -> {
                // Check if sender has already sent request to receiver
                if (relationship == null) {
                    return ResponseEntity.badRequest().body(Response.builder()
                            .message("You haven't sent request to this user!")
                            .status(false)
                            .build());
                }

                // Delete relationship
                relationshipService.delete(relationship);
                return ResponseEntity.ok(Response.builder()
                        .message("Cancel request successfully!")
                        .status(true)
                        .build());
            }
            // Unfriend request
            case UNFRIEND -> {
                // Check if sender has already sent request to receiver
                if (relationship == null) {
                    return ResponseEntity.badRequest().body(Response.builder()
                            .message("You haven't sent request to this user!")
                            .status(false)
                            .build());
                }

                // Delete relationship
                relationshipService.delete(relationship);

                // Send notification to receiver
                String notificationContent = sender.getLastName() + " unfriend you!";
                Notification notification = notificationService.createNotification(notificationContent, NotificationStatusEnum.WARNING);
                messagingTemplate.convertAndSendToUser(receiver.getId().toString(), "/queue/notifications", notification);

                return ResponseEntity.ok(Response.builder()
                        .message("Unfriend successfully!")
                        .status(true)
                        .build());
            }
        }

        return ResponseEntity.badRequest().body(Response.builder()
                .message("Something went wrong!")
                .status(false)
                .build());
    }

    @GetMapping(value = "/get-friends-online", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getFriendsOnline(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        User user = userService.getUserByEmail(getEmailFromToken(authorizationHeader));
        if (user == null) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("User not found!")
                    .status(false)
                    .build());
        }
        List<String> friendsEmail = simpUserRegistry.getUsers().stream().map(SimpUser::getName).collect(Collectors.toList());

        return ResponseEntity.ok(Response.builder()
                .message("Get friends online successfully!")
                .status(true)
                .data(friendsEmail)
                .build());
    }
}
