package com.dev.social.backend.controllers;

import com.dev.social.backend.configs.jwt.TokenManager;
import com.dev.social.backend.dtos.UserDto;
import com.dev.social.backend.enums.RelationshipStatusEnum;
import com.dev.social.backend.enums.RoleCodeEnum;
import com.dev.social.backend.models.Role;
import com.dev.social.backend.models.User;
import com.dev.social.backend.requests.LoginRequest;
import com.dev.social.backend.requests.RegisterRequest;
import com.dev.social.backend.responses.LoginResponse;
import com.dev.social.backend.responses.Response;
import com.dev.social.backend.services.helpers.Converter;
import com.dev.social.backend.services.role.RoleService;
import com.dev.social.backend.services.user.UserService;
import com.dev.social.backend.services.userDetails.UserDetailsServiceImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final UserDetailsServiceImpl userDetailsService;
    private final UserService userService;
    private final RoleService roleService;
    private final TokenManager tokenManager;
    private final PasswordEncoder passwordEncoder;
    private final Converter converter;

    public AuthController(UserDetailsServiceImpl userDetailsService, UserService userService, RoleService roleService, TokenManager tokenManager, PasswordEncoder passwordEncoder, Converter converter) {
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.roleService = roleService;
        this.tokenManager = tokenManager;
        this.passwordEncoder = passwordEncoder;
        this.converter = converter;
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> login(@RequestBody LoginRequest request) {
        if (request.isEmpty()) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("Email or password is empty")
                    .status(false)
                    .build());
        }
        String email = request.getEmail();
        String password = request.getPassword();
        Boolean remember = request.getRemember();
        User user = userService.getUserByEmail(email);
        if (user == null) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("User not found!")
                    .status(false)
                    .build());
        }
        if (!passwordEncoder.matches(password, user.getPassword())) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("Username or Password is incorrect!")
                    .status(false)
                    .build());
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        String token = tokenManager.generateToken(userDetails, remember);
        UserDto userDto = converter.convertUserToUserDto(user);
        return ResponseEntity.ok(Response.builder()
                .message("Login successfully!")
                .status(true)
                .data(LoginResponse.builder()
                        .token(token)
                        .user(userDto).build()
                ).build()
        );
    }

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> register(@RequestBody RegisterRequest request) {
        if (request.isEmpty()) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("All fields are required!")
                    .status(false)
                    .build());
        }
        String email = request.getEmail();
        String phoneNumber = request.getPhoneNumber();
        if (userService.getUserByEmail(email) != null) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("Email is already taken!")
                    .status(false)
                    .build());
        }
        if (userService.getUserByPhoneNumber(phoneNumber) != null) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("Phone number is already taken!")
                    .status(false)
                    .build());
        }
        Role role = roleService.getByRoleCode(RoleCodeEnum.ROLE_USER);
        User user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(email)
                .phoneNumber(phoneNumber)
                .birthDate(request.getBirthDate())
                .password(passwordEncoder.encode(request.getPassword()))
                .profilePicture(request.getProfilePicture())
                .gender(request.getGender())
                .role(role)
                .relationshipStatus(RelationshipStatusEnum.NONE)
                .build();
        userService.save(user);
        return ResponseEntity.ok(Response.builder()
                .message("Register successfully!")
                .status(true)
                .build()
        );
    }

    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getProfile(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("Authorization header is invalid!")
                    .status(false)
                    .build());
        }
        String token = authorizationHeader.substring(7);
        String email = tokenManager.getUsernameFromToken(token);
        User user = userService.getUserByEmail(email);
        if (user == null) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("User not found!")
                    .status(false)
                    .build());
        }
        UserDto userDto = converter.convertUserToUserDto(user);
        return ResponseEntity.ok(Response.builder()
                .message("Get profile successfully!")
                .status(true)
                .data(userDto)
                .build()
        );
    }
}
