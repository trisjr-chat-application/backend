package com.dev.social.backend.controllers;

import com.dev.social.backend.responses.FileResponse.UploadFileDataResponse;
import com.dev.social.backend.responses.Response;
import com.dev.social.backend.services.file.FileStoreService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Log4j2
@RestController
@RequestMapping("/api/file")
public class FileController {
    private final FileStoreService fileService;

    public FileController(FileStoreService fileService) {
        this.fileService = fileService;
    }

    @PostMapping(value = "/convert-to-base64", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> convertToBase64(@RequestBody MultipartFile photo) throws IOException {
        if (photo.isEmpty()) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("File is empty")
                    .status(false)
                    .build());
        }
        String fileContentType = photo.getContentType();
        String base64 = "data:" + fileContentType + ";base64," + fileService.convertToBase64(photo.getBytes());
        return ResponseEntity.ok(Response.builder()
                .data(base64)
                .status(true)
                .message("Convert to base64 successfully!")
                .build());
    }

    @PostMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> upload(@RequestBody MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().body(Response.builder()
                    .message("File is empty")
                    .status(false)
                    .build());
        }
        UploadFileDataResponse response = fileService.uploadFile(file);
        return ResponseEntity.ok(Response.builder()
                .data(response)
                .status(true)
                .message("Upload file successfully!")
                .build());
    }
}
