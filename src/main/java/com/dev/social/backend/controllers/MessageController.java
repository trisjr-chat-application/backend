package com.dev.social.backend.controllers;

import com.dev.social.backend.dtos.MessageDto;
import com.dev.social.backend.dtos.UserAndRoomDto;
import com.dev.social.backend.models.Message;
import com.dev.social.backend.models.Room;
import com.dev.social.backend.models.User;
import com.dev.social.backend.models.UserAndRoom;
import com.dev.social.backend.requests.MessageRequest.GetMessagesRequest;
import com.dev.social.backend.responses.MessageResponse.GetChatDetailsResponse;
import com.dev.social.backend.responses.MessageResponse.GetChatListResponse;
import com.dev.social.backend.responses.MessageResponse.GetMessagesResponse;
import com.dev.social.backend.responses.MessageResponse.ReadMessageResponse;
import com.dev.social.backend.responses.Response;
import com.dev.social.backend.services.helpers.Converter;
import com.dev.social.backend.services.message.MessageService;
import com.dev.social.backend.services.room.RoomService;
import com.dev.social.backend.services.room.UserAndRoomService;
import com.dev.social.backend.services.user.UserService;
import com.dev.social.backend.types.ChatItem;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("/api/message")
public class MessageController {
    private final UserService userService;
    private final MessageService messageService;
    private final RoomService roomService;
    private final UserAndRoomService userAndRoomService;
    private final Converter converter;
    private final SimpMessagingTemplate messagingTemplate;

    @SneakyThrows
    @GetMapping("/chat-list")
    public ResponseEntity<Response> getChatList(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestParam String key) {
        User user = userService.getUserByAuthorizationHeader(authorizationHeader);

        // Get chat list
        List<Room> rooms = userAndRoomService.getRoomsByUser(user);
        List<ChatItem> chatList = new ArrayList<>();
        for (Room room : rooms) {
            ChatItem chatItem = roomService.getChatItem(user, room);
            chatList.add(chatItem);
        }

        // Remove rooms that don't match the key
        if (!key.isEmpty()) {
            chatList = chatList.stream()
                    .filter(chatItem -> chatItem.getKey().contains(key))
                    .collect(Collectors.toList());
        }

        // Sort by last message
        chatList.sort((o1, o2) -> o2.getLastMessage().getCreatedAt().compareTo(o1.getLastMessage().getCreatedAt()));
        return ResponseEntity.ok(
                Response.builder()
                        .message("Chat list fetched successfully")
                        .status(true)
                        .data(GetChatListResponse.builder()
                                .chatList(chatList)
                                .build())
                        .build()
        );
    }


    @GetMapping("/chat-detail")
    public ResponseEntity<Response> getChatDetail(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestParam String key) {
        User user = userService.getUserByAuthorizationHeader(authorizationHeader);
        Pageable pageable = PageRequest.of(0, 30);

        Room room = roomService.getRoomByKey(key);
        if (!userAndRoomService.isUserInRoom(user, room))
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .message("You are not in this room")
                            .status(false)
                            .build()
            );
        // Update last seen
        UserAndRoom userAndRoom = userAndRoomService.getUserAndRoomByUserAndRoom(user, room);
        userAndRoom.setLastSeenAt(new Date());
        userAndRoomService.save(userAndRoom);

        List<Message> messages = messageService.getAllMessagesToRoom(room, pageable);
        List<UserAndRoomDto> users = userAndRoomService.getUserAndRoomByRoom(room).stream()
                .map(converter::convertUserAndRoomToUserAndRoomDto)
                .toList();
        GetChatDetailsResponse response = GetChatDetailsResponse.builder()
                .chatDetail(converter.convertRoomToRoomDto(room, user))
                .messages(messages.stream().map(converter::convertMessageToMessageDto).collect(Collectors.toList()))
                .users(users)
                .build();

        ReadMessageResponse readMessageResponse = ReadMessageResponse.builder()
                .roomKey(room.getRoomKey())
                .users(users)
                .build();
        users.forEach(userAndRoomDto ->
                messagingTemplate.convertAndSendToUser(userAndRoomDto.getUser().getId().toString(), "/queue/read", readMessageResponse)
        );

        return ResponseEntity.ok(
                Response.builder()
                        .message("Chat details fetched successfully")
                        .status(true)
                        .data(response)
                        .build()
        );
    }

    @PostMapping("/messages")
    public ResponseEntity<Response> getChatMessages(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorizationHeader, @RequestBody GetMessagesRequest request) {
        // Get user
        User sender = userService.getUserByAuthorizationHeader(authorizationHeader);

        // Get room
        String key = request.getKey();
        Room room = roomService.getRoomByKey(key);

        // Check if user is in room
        if (!userAndRoomService.isUserInRoom(sender, room))
            return ResponseEntity.badRequest().body(
                    Response.builder()
                            .message("You are not in this room")
                            .status(false)
                            .build()
            );

        // Get messages
        Pageable pageable = PageRequest.of(request.getPage() - 1, request.getPerPage());
        List<MessageDto> messages = messageService.getAllMessagesToRoom(room, pageable).stream()
                .map(converter::convertMessageToMessageDto)
                .collect(Collectors.toList());

        GetMessagesResponse response = new GetMessagesResponse(messages);

        return ResponseEntity.ok(
                Response.builder()
                        .message("Messages fetched successfully")
                        .status(true)
                        .data(response)
                        .build()
        );
    }
}
