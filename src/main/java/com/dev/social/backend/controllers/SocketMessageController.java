package com.dev.social.backend.controllers;


import com.dev.social.backend.dtos.MessageDto;
import com.dev.social.backend.dtos.UserAndRoomDto;
import com.dev.social.backend.models.*;
import com.dev.social.backend.outputs.CreateMessageOutput;
import com.dev.social.backend.requests.MessageRequest.ReactionMessageRequest;
import com.dev.social.backend.requests.MessageRequest.ReadMessageRequest;
import com.dev.social.backend.requests.MessageRequest.SendMessageRequest;
import com.dev.social.backend.responses.MessageResponse.ReadMessageResponse;
import com.dev.social.backend.services.helpers.Converter;
import com.dev.social.backend.services.message.MessageService;
import com.dev.social.backend.services.message.MessageStatusService;
import com.dev.social.backend.services.room.RoomService;
import com.dev.social.backend.services.room.UserAndRoomService;
import com.dev.social.backend.services.user.UserService;
import com.dev.social.backend.types.SendMessage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.Date;
import java.util.List;

@Controller
@Slf4j
@AllArgsConstructor
public class SocketMessageController {
    private final SimpMessagingTemplate messagingTemplate;
    private final UserService userService;
    private final MessageService messageService;
    private final MessageStatusService messageStatusService;
    private final Converter converter;
    private final RoomService roomService;
    private final UserAndRoomService userAndRoomService;

    public void sendMessageToUsers(List<User> users, MessageDto message, String roomKey) {
        Room room = roomService.getRoomByKey(roomKey);
        SendMessage sendMessage = SendMessage.builder()
                .message(message)
                .build();
        users.forEach(user -> {
            sendMessage.setRoom(converter.convertRoomToRoomDto(room, user));
            messagingTemplate.convertAndSendToUser(user.getId().toString(), "/queue/messages", sendMessage);
        });
    }

    public void sendReactionToUsers(List<User> users, MessageDto message) {
        users.forEach(user -> messagingTemplate.convertAndSendToUser(user.getId().toString(), "/queue/reactions", message));
    }

    @MessageMapping({"/send"})
    public void sendMessage(@Header(HttpHeaders.AUTHORIZATION) String authorizationHeader, @Payload SendMessageRequest request) {
        User sender = userService.getUserByAuthorizationHeader(authorizationHeader);
        CreateMessageOutput output = messageService.createMessage(sender, request);

        List<User> users = output.getUsers();
        MessageDto message = converter.convertMessageToMessageDto(messageService.getMessageById(output.getMessage().getId()));

        // Send message to all users
        sendMessageToUsers(users, message, output.getRoomKey());
    }

    @MessageMapping({"reaction"})
    public void reactionMessage(@Header(HttpHeaders.AUTHORIZATION) String authorizationHeader, @Payload ReactionMessageRequest request) {
        User sender = userService.getUserByAuthorizationHeader(authorizationHeader);
        Message message = messageService.getMessageById(request.getMessageId());
        MessageStatus messageStatus = messageStatusService.getMessageStatusByUserAndMessage(sender, message);
        if (messageStatus == null) {
            messageStatus = messageStatusService.createMessageStatus(sender, message);
        }

        messageStatus.setReaction(request.getReaction());
        messageStatusService.save(messageStatus);
        Message savedMessage = messageService.getMessageById(request.getMessageId());
        List<User> users = savedMessage.getRoom().getUsersInvited().stream().map(UserAndRoom::getUser).toList();

        sendReactionToUsers(users, converter.convertMessageToMessageDto(savedMessage));
    }

    @MessageMapping({"read"})
    public void readMessage(@Header(HttpHeaders.AUTHORIZATION) String authorizationHeader, @Payload ReadMessageRequest request) {
        User sender = userService.getUserByAuthorizationHeader(authorizationHeader);
        Room room = roomService.getRoomByKey(request.getRoomKey());

        UserAndRoom userAndRoom = userAndRoomService.getUserAndRoomByUserAndRoom(sender, room);
        Date lastSeenAt = new Date();
        userAndRoom.setLastSeenAt(lastSeenAt);
        userAndRoomService.save(userAndRoom);

        List<UserAndRoomDto> users = userAndRoomService.getUserAndRoomByRoom(room).stream()
                .map(converter::convertUserAndRoomToUserAndRoomDto)
                .toList();

        ReadMessageResponse response = ReadMessageResponse.builder()
                .roomKey(request.getRoomKey())
                .users(users)
                .build();
        messagingTemplate.convertAndSendToUser(sender.getId().toString(), "/queue/read", response);
    }
}