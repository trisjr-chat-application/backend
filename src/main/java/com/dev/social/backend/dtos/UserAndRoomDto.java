package com.dev.social.backend.dtos;

import com.dev.social.backend.enums.MemberGroupStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the {@link com.dev.social.backend.models.UserAndRoom} entity
 */
@Data
public class UserAndRoomDto implements Serializable {
    private final Long id;
    private final UserDto user;
    private final Boolean isCreator;
    private final MemberGroupStatusEnum status;
    private final Date lastSeenAt;
    private final Date createdAt;
    private final Date updatedAt;
}