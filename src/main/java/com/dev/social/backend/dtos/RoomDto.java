package com.dev.social.backend.dtos;

import com.dev.social.backend.enums.RoomTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * A DTO for the {@link com.dev.social.backend.models.Room} entity
 */
@Data
public class RoomDto implements Serializable {
    private final Long id;
    private final String roomKey;
    private final String name;
    private final String image;
    private final RoomTypeEnum type;
    private final Set<UserAndRoomDto> usersInvited;
    private final Date createdAt;
    private final Date updatedAt;
}