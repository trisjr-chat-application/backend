package com.dev.social.backend.dtos;

import com.dev.social.backend.models.Relationship;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the {@link Relationship} entity
 */
@Data
public class RelationshipDto implements Serializable {
    private final Long id;
    private final UserDto sender;
    private final UserDto receiver;
    private final Boolean isAccepted;
    private final Date createdAt;
    private final Date updatedAt;
}