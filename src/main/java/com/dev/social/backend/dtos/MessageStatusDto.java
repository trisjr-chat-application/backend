package com.dev.social.backend.dtos;

import com.dev.social.backend.enums.ReactionEnum;
import com.dev.social.backend.models.MessageStatus;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the {@link MessageStatus} entity
 */
@Data
public class MessageStatusDto implements Serializable {
    private final Long id;
    private final UserDto user;
    private final ReactionEnum reaction;
    private final Date createdAt;
    private final Date updatedAt;
}