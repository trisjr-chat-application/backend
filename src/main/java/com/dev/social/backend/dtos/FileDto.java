package com.dev.social.backend.dtos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the {@link com.dev.social.backend.models.File} entity
 */
@Data
public class FileDto implements Serializable {
    private final Long id;
    private final String name;
    private final String path;
    private final String type;
    private final String url;
    private final Long size;
    private final Date limitTime;
    private final Date createdAt;
    private final Date updatedAt;
}