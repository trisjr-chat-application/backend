package com.dev.social.backend.dtos;

import com.dev.social.backend.enums.GenderEnum;
import com.dev.social.backend.enums.RelationshipStatusEnum;
import com.dev.social.backend.models.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the {@link User} entity
 */
@Data
public class UserDto implements Serializable {
    private final Long id;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String phoneNumber;
    private final Date birthDate;
    private final String address;
    private final String city;
    private final String country;
    private final String profilePicture;
    private final String coverPicture;
    private final String aboutMe;
    private final GenderEnum gender;
    private final RelationshipStatusEnum relationshipStatus;
}