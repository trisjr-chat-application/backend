package com.dev.social.backend.dtos;

import com.dev.social.backend.enums.RoleCodeEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * A DTO for the {@link com.dev.social.backend.models.Role} entity
 */
@Data
public class RoleDto implements Serializable {
    private final Long id;
    private final RoleCodeEnum roleCode;
}