package com.dev.social.backend.dtos;

import com.dev.social.backend.enums.MessageEnum;
import com.dev.social.backend.models.File;
import com.dev.social.backend.types.UrlPreview;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
public class MessageDto implements Serializable {
    private Long id;
    private String content;
    private UserDto sender;
    private String roomKey;
    private MessageEnum type;
    private MessageDto replyMessage;
    private Set<MessageStatusDto> messageStatuses;
    private Date createdAt;
    private Date updatedAt;
    private UrlPreview urlPreview;
    private File file;
}
